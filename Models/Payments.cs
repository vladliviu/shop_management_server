﻿using System;
using System.Collections.Generic;

namespace ShopManagement.Models
{
    public partial class Payments
    {
        public Payments()
        {
            Staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string Workingdays { get; set; }
        public double? TicketValue { get; set; }
        public int? TicketNo { get; set; }
        public double? Salary { get; set; }
        public double? Bonus { get; set; }

        public virtual ICollection<Staff> Staff { get; set; }
    }
}
