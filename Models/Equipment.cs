﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Equipment
    {
        public Equipment()
        {
            Downtimes = new HashSet<Downtimes>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Downtimes> Downtimes { get; set; }
    }
}
