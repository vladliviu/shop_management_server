﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Staffoccupation
    {
        public Staffoccupation()
        {
            Staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Staff> Staff { get; set; }
    }
}
