﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Staff
    {
        public Staff()
        {
            Downtimes = new HashSet<Downtimes>();
            Notification = new HashSet<Notification>();
            Orderproduct = new HashSet<Orderproduct>();
        }

        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        [Required]
        public int? StaffoccupationId { get; set; }
        [Required]
        public int? PaymentsId { get; set; }

        public virtual Payments Payments { get; set; }
        public virtual Staffoccupation Staffoccupation { get; set; }
        public virtual ICollection<Downtimes> Downtimes { get; set; }
        public virtual ICollection<Notification> Notification { get; set; }
        public virtual ICollection<Orderproduct> Orderproduct { get; set; }
    }
}
