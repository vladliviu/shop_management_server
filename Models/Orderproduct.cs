﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Orderproduct
    {
        public int Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Description { get; set; }
        [Required]
        public int? Quantity { get; set; }
        public double? Price { get; set; }
        public int? StaffId { get; set; }
        [Required]
        public int? ProductId { get; set; }
        [Required]
        public int? SupplierId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}
