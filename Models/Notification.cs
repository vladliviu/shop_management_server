﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Notification
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        [Required]
        public string Type { get; set; }
        public string Description { get; set; }
        public int? StaffId { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
