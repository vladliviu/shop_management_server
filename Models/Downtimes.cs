﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Downtimes
    {
        public int Id { get; set; }
        [Required]
        public string Type { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime? From { get; set; }
        [Required]
        public DateTime? To { get; set; }
        [Required]
        public int? StaffId { get; set; }
        [Required]
        public int? EquipmentId { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual Staff Staff { get; set; }
    }
}
