﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Orderproduct = new HashSet<Orderproduct>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Details { get; set; }
        [Required]
        public string Address { get; set; }

        public virtual ICollection<Orderproduct> Orderproduct { get; set; }
    }
}
