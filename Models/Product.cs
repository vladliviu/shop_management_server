﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopManagement.Models
{
    public partial class Product
    {
        public Product()
        {
            Orderproduct = new HashSet<Orderproduct>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public double? Price { get; set; }
        [Required]
        public double? Weight { get; set; }
        [Required]
        public string Category { get; set; }
        public int? Quantity { get; set; }
        public int? Availableqty { get; set; }

        public virtual ICollection<Orderproduct> Orderproduct { get; set; }
    }
}
