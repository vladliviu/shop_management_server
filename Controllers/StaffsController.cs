﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ShopManagement.Models;
using ShopManagement.ViewModels;

namespace ShopManagement.Controllers
{
    public class StaffsController : Controller
    {
        private readonly shop_managerContext _context;

        public StaffsController(shop_managerContext context)
        {
            
            _context = context;
        }

        public IActionResult Login()
        {
           // TempData.Keep("staffoccupationID");
            TempData["staffoccupationID"] = 0;
            TempData["staffusername"] = "";
            TempData["staffid"] = 0;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("Username,Password")] Staff login)
        {
            var loginInfo = _context.Staff.Where(s => s.Username == login.Username && s.Password == login.Password).FirstOrDefault();

            if (loginInfo == null)
            {
                return (RedirectToAction(nameof(Login)));
            }
            else
            {
                TempData["staffoccupationID"] = loginInfo.StaffoccupationId;
                TempData["tsusername"] = loginInfo.Username;
                TempData["staffid"] = loginInfo.Id;
                return RedirectToAction("Details", new { id = loginInfo.Id });
            }
        }

        public IActionResult Logout()
        {
            return RedirectToAction(nameof(Login));
        }

        // GET: Staffs
        public async Task<IActionResult> Index(string searchStaff)
        {
            var shop_managerContext = from s in _context.Staff select s;

            if (!String.IsNullOrEmpty(searchStaff))
            {
                shop_managerContext = shop_managerContext.Include(s => s.Payments).Include(s => s.Staffoccupation).Where(s => s.Username.Contains(searchStaff));
                
            }
            return View(await shop_managerContext.ToListAsync());
        }

        // GET: Staffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .Include(s => s.Payments)
                .Include(s => s.Staffoccupation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // GET: Staffs/Create
        public IActionResult Create()
        {
            ViewData["PaymentsId"] = new SelectList(_context.Payments, "Id", "Id");
            ViewData["StaffoccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Id");
            return View();
        }

        // POST: Staffs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Password,Firstname,Lastname,Email,Phone,Address,StaffoccupationId,PaymentsId")] Staff staff)
        {
            if (ModelState.IsValid)
            {
                _context.Add(staff);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PaymentsId"] = new SelectList(_context.Payments, "Id", "Id", staff.PaymentsId);
            ViewData["StaffoccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Id", staff.StaffoccupationId);
            return View(staff);
        }

        // GET: Staffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff.FindAsync(id);
            if (staff == null)
            {
                return NotFound();
            }
            ViewData["PaymentsId"] = new SelectList(_context.Payments, "Id", "Id", staff.PaymentsId);
            ViewData["StaffoccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Id", staff.StaffoccupationId);
            return View(staff);
        }

        // POST: Staffs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password,Firstname,Lastname,Email,Phone,Address,StaffoccupationId,PaymentsId")] Staff staff)
        {

            if (id != staff.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(staff);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffExists(staff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PaymentsId"] = new SelectList(_context.Payments, "Id", "Id", staff.PaymentsId);
            ViewData["StaffoccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Id", staff.StaffoccupationId);
            return View(staff);
        }

        // GET: Staffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .Include(s => s.Payments)
                .Include(s => s.Staffoccupation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // POST: Staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staff = await _context.Staff.FindAsync(id);
            _context.Staff.Remove(staff);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StaffExists(int id)
        {
            return _context.Staff.Any(e => e.Id == id);
        }
    }
}
