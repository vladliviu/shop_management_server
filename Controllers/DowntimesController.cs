﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ShopManagement.Models;

namespace ShopManagement.Controllers
{
    public class DowntimesController : Controller
    {
        private readonly shop_managerContext _context;

        public DowntimesController(shop_managerContext context)
        {
            _context = context;
        }

        // GET: Downtimes
        public async Task<IActionResult> Index()
        {
                var shop_managerContext = _context.Downtimes.Include(d => d.Equipment).Include(d => d.Staff);
                return View(await shop_managerContext.ToListAsync());
        }

        // GET: Downtimes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtimes = await _context.Downtimes
                .Include(d => d.Equipment)
                .Include(d => d.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (downtimes == null)
            {
                return NotFound();
            }

            return View(downtimes);
        }

        // GET: Downtimes/Create
        public IActionResult Create()
        {
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name");
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            return View();
        }

        // POST: Downtimes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Type,Description,From,To,StaffId,EquipmentId")] Downtimes downtimes)
        {
            if (ModelState.IsValid)
            {
                _context.Add(downtimes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtimes.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtimes.StaffId);
            return View(downtimes);
        }

        // GET: Downtimes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtimes = await _context.Downtimes.FindAsync(id);
            if (downtimes == null)
            {
                return NotFound();
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtimes.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtimes.StaffId);
            return View(downtimes);
        }

        // POST: Downtimes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Type,Description,From,To,StaffId,EquipmentId")] Downtimes downtimes)
        {
            if (id != downtimes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(downtimes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DowntimesExists(downtimes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtimes.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtimes.StaffId);
            return View(downtimes);
        }

        // GET: Downtimes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtimes = await _context.Downtimes
                .Include(d => d.Equipment)
                .Include(d => d.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (downtimes == null)
            {
                return NotFound();
            }

            return View(downtimes);
        }

        // POST: Downtimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var downtimes = await _context.Downtimes.FindAsync(id);
            _context.Downtimes.Remove(downtimes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DowntimesExists(int id)
        {
            return _context.Downtimes.Any(e => e.Id == id);
        }
    }
}
