﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ShopManagement.Models;

namespace ShopManagement.Controllers
{
    public class OrderproductsController : Controller
    {
        private readonly shop_managerContext _context;
        private readonly ProductsController _productsController;

        public OrderproductsController(shop_managerContext context, ProductsController productsController)
        {
            _context = context;
            _productsController = productsController;
        }

        // GET: Orderproducts
        public async Task<IActionResult> Index()
        {
            var shop_managerContext = _context.Orderproduct.Include(o => o.Product).Include(o => o.Staff).Include(o => o.Supplier);
            return View(await shop_managerContext.ToListAsync());
        }

        // GET: Orderproducts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderproduct = await _context.Orderproduct
                .Include(o => o.Product)
                .Include(o => o.Staff)
                .Include(o => o.Supplier)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderproduct == null)
            {
                return NotFound();
            }

            return View(orderproduct);
        }

        // GET: Orderproducts/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name");
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name");
            return View();
        }

        // POST: Orderproducts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CreatedAt,Description,Quantity,Price,StaffId,ProductId,SupplierId")] Orderproduct orderproduct)
        {
            if (ModelState.IsValid)
            {
                orderproduct.CreatedAt = DateTime.Now;
                var oPrice = _context.Product.Where(p => p.Id == orderproduct.ProductId).FirstOrDefault();
                orderproduct.Price = orderproduct.Quantity * oPrice.Price;
                orderproduct.StaffId = (int)TempData["staffid"];
                _context.Add(orderproduct);
                oPrice.Quantity += orderproduct.Quantity;
                oPrice.Availableqty += orderproduct.Quantity;
                _productsController.Edit(oPrice.Id, oPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", orderproduct.ProductId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", orderproduct.StaffId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name", orderproduct.SupplierId);
            return View(orderproduct);
        }

        // GET: Orderproducts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderproduct = await _context.Orderproduct.FindAsync(id);
            if (orderproduct == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", orderproduct.ProductId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", orderproduct.StaffId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name", orderproduct.SupplierId);
            return View(orderproduct);
        }

        // POST: Orderproducts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CreatedAt,Description,Quantity,Price,StaffId,ProductId,SupplierId")] Orderproduct orderproduct)
        {
            if (id != orderproduct.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orderproduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderproductExists(orderproduct.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", orderproduct.ProductId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", orderproduct.StaffId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name", orderproduct.SupplierId);
            return View(orderproduct);
        }

        // GET: Orderproducts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderproduct = await _context.Orderproduct
                .Include(o => o.Product)
                .Include(o => o.Staff)
                .Include(o => o.Supplier)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderproduct == null)
            {
                return NotFound();
            }

            return View(orderproduct);
        }

        // POST: Orderproducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var orderproduct = await _context.Orderproduct.FindAsync(id);
            _context.Orderproduct.Remove(orderproduct);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderproductExists(int id)
        {
            return _context.Orderproduct.Any(e => e.Id == id);
        }
    }
}
